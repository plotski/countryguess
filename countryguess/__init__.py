__project_name__ = 'countryguess'
__version__ = '0.4.5'

from ._countrydata import CountryData
from ._guess_country import guess_country
